#ifndef _buzzwd_
#define _buzzwd_

#define BUF_SZ 256
#define WORD_SZ 64 // no word can be longer, or program fails
#define BUZZWD_N 10 // number of buzzwords
#define MIN_CHANGE_SZ 5 // words must be this long to get replaced by buzzwords

// All the symbols that come before a word, such as spaces, newlines etc.
#define PREWORD_SYMS " \n\xA\xC\xD\"'"
// Punctuation symbols
#define PUNCT_SYMS ".,?/;:!"

#define LCAPITAL 0 // capital letter
#define LLOWERCASE 1 // lowercase letter
#define LPREWORD 2 // character that can come immediately before the start of a word, eg <spc> or " or '
#define LPUNCTUATION 3
#define LUNKNOWN 4

#define RANDOMISE_WORDS 1

//#define BUZZWD_DEBUG 1

typedef struct word_{
    char letters[WORD_SZ];
    uint sz;
} Word;

// Functions
void usage();
void buzzwds_init(Word buzzwds[], int num);
int  char_type(char c);
int write_random_wd(char orig_wd[], Word buzzwd[], int firstcap, FILE *f);
  
#endif
