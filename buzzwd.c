/* Text to buzzword
 * Vlad Negnevitsky, 2012
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "buzzwd.h"

int main(int argc, char *argv[]){

    char filename_in[BUF_SZ], filename_out[BUF_SZ];
    char k;
    Word buzzwds[BUZZWD_N];

    FILE *fin, *fout;

    if (argc<2 || argc>3){
	// Too few or many input params
	usage();
	exit(EXIT_SUCCESS);
    } else {
	buzzwds_init(buzzwds, BUZZWD_N);
    
	// Input filename
	strcpy(filename_in,argv[1]);

	// Output filename
	if (argc == 2) {
	    strcpy(filename_out,"out.txt");
	} else {
	    strcpy(filename_out,argv[2]);
	}
    }

    fin = fopen(filename_in,"r");
    if (fin == NULL){
	printf("Error opening input file: %s\n",filename_in);
	exit(EXIT_FAILURE);
    }

    fout = fopen(filename_out,"w");
    if (fout == NULL){
	printf("Error opening output file: %s\n",filename_out);
	exit(EXIT_FAILURE);
    }

    // printf("Str %s, str %s\n",filename_in, filename_out);

    // printf("%d\n",char_type(' '));

    // Parsing loop
  
    // Parser states
    int in_word=0, word_len=0, capitalise=0;
    int type_prev = LPREWORD, type = 0;
    char temp_word[MIN_CHANGE_SZ+2];

    while ( (k=fgetc(fin)) != EOF){
	type = char_type(k);
#ifdef BUZZWD_DEBUG
	printf("%c:   ",k);
#endif    

	if (type > LLOWERCASE){ // not a letter
	    if (in_word){
#ifdef BUZZWD_DEBUG
		printf("Writing out immediately: ");
#endif	  
		// Write out current word immediately
		if (word_len>= MIN_CHANGE_SZ){
		    // Long enough to write out a buzzword
#ifdef BUZZWD_DEBUG
		    printf("buzzword");
#endif	  	  
		    write_random_wd(temp_word, buzzwds, capitalise, fout);
		} else {
		    // Not long enough to write out a buzzword
		    fwrite(temp_word, word_len, 1, fout);
#ifdef BUZZWD_DEBUG
		    printf("orig text");
#endif	  
		}
		word_len = 0;
		in_word = 0;
		capitalise = 0;
	    }
	    fputc(k, fout);
	} else {
#ifdef BUZZWD_DEBUG
	    printf("a letter; ");
#endif      
	    // a letter
	
	    if (in_word){
#ifdef BUZZWD_DEBUG
		printf("extending current word");
#endif      		
		temp_word[word_len++] = k;
	    } else if (type_prev == LPREWORD){
#ifdef BUZZWD_DEBUG
		printf("starting new word");
#endif      	
		// starting a new word
		in_word = 1;
		temp_word[word_len++] = k;
		capitalise = (type == LCAPITAL);
	    } else {
		// word started with a non-starting character, so leave it alone
#ifdef BUZZWD_DEBUG
		printf("non-usable word");
#endif  	
		fputc(k, fout);
	    }
	}
      
	type_prev = type;
#ifdef BUZZWD_DEBUG
	printf("\n");
#endif
    } 

    fclose(fin);
    fclose(fout);
    exit(EXIT_SUCCESS);
}

void usage(){
    printf("Usage: buzzwd FILE [FILEOUT]\n");
    printf("\tConverts an input text file into a list of buzzwords.\n\n");
    printf("\tFILE: input text file\n");
    printf("\tFILEOUT: output text file (default: out.txt)\n\n");
    printf("\tFor example: ./buzzwd input.txt output.txt\n");
}

void buzzwds_init(Word buzzwds[], int num){
    int k;

    strcpy(buzzwds[0].letters,"python");
    strcpy(buzzwds[1].letters,"numpy");
    strcpy(buzzwds[2].letters,"matplotlib");
    strcpy(buzzwds[3].letters,"GTK");
    strcpy(buzzwds[4].letters,"pylab");
    strcpy(buzzwds[5].letters,"pyGTK");
    strcpy(buzzwds[6].letters,"pyplot");
    strcpy(buzzwds[7].letters,"multithreading");
    strcpy(buzzwds[8].letters,"hdf5");
    strcpy(buzzwds[9].letters,"pandas");

    for(k=0;k<num;++k){
	buzzwds[k].sz=strlen(buzzwds[k].letters);
    }
}

int char_type(char ch){
    // Figure out what type of character ch is
  
    char preword_syms[] = PREWORD_SYMS;
    char punct_syms[] = PUNCT_SYMS;
    int k;

    if(ch>64 && ch<91){
	return LCAPITAL;
    } else if(ch>96 && ch<123){
	return LLOWERCASE;
    }
  
    for(k=0;k<strlen(punct_syms);++k){
	if (ch == punct_syms[k]){
	    return LPUNCTUATION;
	}
    }

    for(k=0;k<strlen(preword_syms);++k){
	if (ch == preword_syms[k]){
	    return LPREWORD;
	}
    }
  
    return LUNKNOWN;
}

int write_random_wd(char orig_wd[], Word buzzwd[], int firstcap, FILE *f){
    int wd;
#ifdef RANDOMISE_WORDS
    // Choose random buzzword
    wd = ( rand() & 0xff ) % BUZZWD_N;
#else
    // Sum up the characters in orig_wd
    int k, orig_wd_sum = 0;
    for (k=0; k<=MIN_CHANGE_SZ;k++){
	orig_wd_sum += orig_wd[k];
    }
  
    // Choose new word based on sum of old one
    int wd = orig_wd_sum % BUZZWD_N;
#endif
  
    if (firstcap && char_type(buzzwd[wd].letters[0])){ // capital => char_type returns 0
	char temp_wd[WORD_SZ];
	strcpy(temp_wd, buzzwd[wd].letters);
	temp_wd[0] -= 32; // convert to capital; ascii
	return fwrite(temp_wd, buzzwd[wd].sz, 1, f);
    } else {
	return fwrite(buzzwd[wd].letters, buzzwd[wd].sz, 1, f);
    }   
}

