# Variables 
CC = gcc
CFLAGS = -I. -O2 -Wall
FILENAME = buzzwd
OBJECTS = buzzwd.o
MAINHEADERS = buzzwd.h

# Rules

all: $(FILENAME)

debug: $(FILENAME)

$(FILENAME): $(OBJECTS)
	$(CC) $(CFLAGS) $(OBJECTS) -o $(FILENAME)
#	strip $(FILENAME)

%.o: %.c $(MAINHEADERS) Makefile
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	rm -f $(OBJECTS) $(FILENAME)

run: all
	./$(FILENAME)
